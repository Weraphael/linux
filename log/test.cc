#include <iostream>
#include <cstdio>
#include <stdarg.h>
#include <errno.h>
#include <time.h>
#include <errno.h>
#include "log.hpp"
#include <string>
#include <cstring>
using namespace std;

int main()
{
    log l;
    l.Enable(OneFile);
    l.logmessage(Info, "错误码: %d, 信息: %s", errno, strerror(errno));
    return 0;
}

// int main()
// {
//     time_t _timestamp = time(NULL); // time函数会返回时间戳
//     // 再将time_t类型转化为struct tm类型
//     struct tm *_tm = localtime(&_timestamp);
//     // 获取：年-月-日:时:分:秒
//     printf("%d-%d-%d:%d:%d:%d\n", _tm->tm_year + 1900, _tm->tm_mon + 1, _tm->tm_mday,
//            _tm->tm_hour, _tm->tm_min, _tm->tm_sec);

//     return 0;
// }

// n - 元素个数
// ... - 可变参数。用户可以传任意的参数
// int Sum(int n, ...)
// {
//     va_list args; // 定义一个存储可变参数的容器
//     int total = 0;

//     va_start(args, n); // 对容器填充数据

//     // 遍历所有传入的参数
//     for (int i = 0; i < n; i++)
//     {
//         total += va_arg(args, int); // 访问可变参数列表中的下一个参数，具体参数类型为int
//     }

//     // 清理资源
//     va_end(args);

//     return total;
// }

// int main()
// {
//     cout << Sum(3, 1, 2, 3) << endl; // 要求3个元素的总和 -> 1 + 2 + 3 = 6

//     return 0;
// }
