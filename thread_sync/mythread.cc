// #include <unistd.h>
// #include <iostream>
// #include <pthread.h>

// using namespace std;

// const int NUM = 5; // 线程个数
// int count = 0;     // 临界资源

// pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER; // 创建锁资源
// pthread_cond_t cond = PTHREAD_COND_INITIALIZER;   // 创建条件变量

// void *thread_task(void *args)
// {
//     // 线程分离：让操作系统回收线程的资源
//     pthread_detach(pthread_self());
//     // 我的Linux机器默认是64位，指针是8字节，因此我使用8字节的long long
//     // 而不是用int是因为会发生截断。
//     long long number = (long long)args; // 线程编号1~5
//     cout << "pthread" << number << "创建成功..." << endl;
//     while (true)
//     {
//         // 加锁
//         pthread_mutex_lock(&lock);
//         // 条件等待
//         pthread_cond_wait(&cond, &lock);
//         cout << "pthread" << number << ": count = " << ++count << endl;
//         // 解锁
//         pthread_mutex_unlock(&lock);
//     }
// }

// ==============================  条件变量 ================================================
// int main()
// {
//     for (long long i = 1; i <= NUM; i++)
//     {
//         pthread_t tid;
//         pthread_create(&tid, nullptr, thread_task, (void *)i);
//         // 通过适当的休眠，可以减少线程之间的竞争条件，即减少因多个线程同时被创建而可能导致的竞争和不确定性。
//         sleep(1);
//     }
//     sleep(3); // 让所有线程都去等待队列中等待
//     cout << "所有线程全部入队列" << endl;

//     // 主线程负责唤醒线程
//     while (true)
//     {
//         // pthread_cond_signal(&cond); // 唤醒等待队列第一个线程
//         pthread_cond_broadcast(&cond);
//         // cout << "主线程唤醒个线程..." << endl;
//         cout << "主线程唤醒了整个队列的线程..." << endl;
//         sleep(1);
//     }

//     return 0;
// }

#include <unistd.h>
#include <iostream>
#include <pthread.h>

using namespace std;

const int NUM = 5; // 线程个数
int count = 0;     // 临界资源

pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER; // 创建锁资源

void *thread_task(void *args)
{
    // 线程分离：让操作系统回收线程的资源
    pthread_detach(pthread_self());
    // 我的Linux机器默认是64位，指针是8字节，因此我使用8字节的long long
    // 而不是用int是因为会发生截断。
    long long number = (long long)args; // 线程编号1~5
    cout << "pthread" << number << "创建成功..." << endl;
    while (true)
    {
        // 加锁
        pthread_mutex_lock(&lock);
        cout << "pthread" << number << ": count = " << ++count << endl;
        // 解锁
        pthread_mutex_unlock(&lock);
    }
}

// ======================= 互斥 =========================
// 目的：查看现象
int main()
{
    for (long long i = 1; i <= NUM; i++)
    {
        pthread_t tid;
        pthread_create(&tid, nullptr, thread_task, (void *)i);
        // 通过适当的休眠，可以减少线程之间的竞争条件，即减少因多个线程同时被创建而可能导致的竞争和不确定性。
        sleep(1);
    }
    sleep(3); // 让所有线程都去等待队列中等待
    cout << "所有线程全部入队列" << endl;

    // 主线程不要退出，不然该进程下的所有线程都退出了
    while (true)
    {
        sleep(1);
    }

    return 0;
}
