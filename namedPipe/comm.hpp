#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <string>
#include <cstring>
using namespace std;

enum
{
    // 规定错误码从1开始递增
    MKFIFO_FAIL = 1, // 创建匿名管道失败
    UNLINK_FAIL,     // 删除匿名管道失败
    OPEN_FAIL        // 打开文件失败
};