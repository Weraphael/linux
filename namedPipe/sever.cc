#include "comm.hpp"
// sever：主要是向管道读取
int main()
{
    // 1. 在当前目录下创建命名管道文件为myfifo
    int n = mkfifo("./myfifo", 0666);
    if (n == -1) // 命名管道创建失败
    {
        perror("mkfifo");
        exit(MKFIFO_FAIL);
    }

    // 2. 读取管道的内容 - 开始通信
    // 首先是打开对应的命名管道文件
    int fd = open("./myfifo", O_RDONLY); // O_RDONLY - 以只读的方式
    if (fd < 0)                          // 文件打开失败
    {
        perror("open");
        exit(OPEN_FAIL);
    }
    // 读取管道文件的内容
    while (true)
    {
        char buffer[1024] = {0};
        int x = read(fd, buffer, sizeof(buffer));
        if (x > 0) // 读取成功
        {
            buffer[x] = 0;
            cout << "client say# " << buffer << endl;

            // strcasecmp 是一个字符串比较函数，无论字符串大小写，都能进行比较
            if (strcasecmp("exit", buffer) == 0) // 如果读取到用户输入exit就退出
            {
                break;
            }
        }
        else if (x == 0) // 管道特性1：写端client关闭, 会给读端的read函数返回0，表示可以取消通信了
        {
            cout << "client quit, me too ~" << endl;
            break;
        }
    }

    // 读取完后关闭文件描述符
    close(fd);

    // 3. 回收管道文件
    int m = unlink("./myfifo"); // unlink函数可以删除目录文件
    if (m == -1)
    {
        perror("unlink");
        exit(UNLINK_FAIL);
    }

    return 0;
}