#include "comm.hpp"

// client：主要是向管道写内容
int main()
{
    // 1. 在向命名管道文件写入数据，首先打开命名管道文件。
    int fd = open("./myfifo", O_WRONLY);
    if (fd < 0) // 文件打开失败
    {
        perror("open");
        exit(OPEN_FAIL);
    }

    // 2. 向命名管道里写内容 - 开始通信
    string input;
    while (true)
    {
        cout << "client请输入: ";
        getline(cin, input);

        write(fd, input.c_str(), input.size());
        if (strcasecmp("exit", input.c_str()) == 0) // 如果读取到用户输入exit就退出
        {
            break;
        }
    }

    // 3. 通信结束后，关闭文件描述符
    close(fd);

    return 0;
}