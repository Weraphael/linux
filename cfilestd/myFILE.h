#pragma once 

#include <string.h>

#define FLUSH_NOW 1 // 立即刷新
#define FLUSH_LINE 2 // 行刷新
#define FLUSH_ALL 4 // 全刷新

typedef struct IO_FILE 
{
    int _fileno; 
    char* outbuffer;
    int out_pos;
    int flag; // 缓冲区的刷新方式
} _FILE;


_FILE* _fopen(const char* filename, const char* mode);

size_t _fwrite(const char *ptr, size_t size, size_t nmemb, _FILE *stream);

void _fclose(_FILE *fp);

int _fflush(_FILE* fp);
