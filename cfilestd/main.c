#include "myFILE.h"
#include <stdio.h>
#include <unistd.h>
int main()
{
    // 我们实现以下接口
    // 为了防止和库里的冲突，我们模拟实现的函数名都以_开头
    
    // 1. 打开文件
    //_FILE* fp = _fopen("log.txt", "a");
    //if (fp == NULL)
    //{
        //return 1;
    //}
    // 2. 向文件写入
    const char* message = "hello FILE\n";
    //_fwrite(fp, message, strlen(message));
    _fwrite(stdout, message, strlen(message));

    sleep(5);
    // 3. 强制刷新缓冲区
//    _fflush(fp);

    // 3. 关闭文件
    //_fclose(fp);

    return 0;
}
