#pragma once

#include <pthread.h>

// class LockGuard
// {
// public:
//     LockGuard(pthread_mutex_t *lock)
//         : _mutex(lock)
//     {
//         pthread_mutex_lock(_mutex);
//     }

//     ~LockGuard()
//     {
//         pthread_mutex_unlock(_mutex);
//     }

// private:
//     pthread_mutex_t *_mutex;
// };

class Mutex
{
public:
    Mutex(pthread_mutex_t *lock) : _lock(lock)
    {
    }
    ~Mutex()
    {
    }

    void Lock() // 加锁
    {
        pthread_mutex_lock(_lock);
    }

    void Unlock() // 解锁
    {
        pthread_mutex_unlock(_lock);
    }

private:
    pthread_mutex_t *_lock;
};

class LockGuard
{
public:
    LockGuard(pthread_mutex_t *lock)
        : _mutex(lock)
    {
        _mutex.Lock();
    }

    ~LockGuard()
    {
        _mutex.Unlock();
    }

private:
    Mutex _mutex;
};

// 主要是为了用对象的生命周期来管理加锁和解锁操作，这种风格我们称为RAII风格的锁
