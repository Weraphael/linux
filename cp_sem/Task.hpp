#pragma once
#include <iostream>

std::string oper = "+-*/";
enum
{
    DivZero = 1,
    Unknow
};

class Task
{
public:
    Task()
    {
    }

    Task(int data1, int data2, char op)
        : _data1(data1), _data2(data2), _op(op), _result(0), _exitcode(0)
    {
    }

    void run()
    {
        switch (_op)
        {
        case '+':
            _result = _data1 + _data2;
            break;

        case '-':
            _result = _data1 - _data2;
            break;

        case '*':
            _result = _data1 * _data2;
            break;

        case '/':
            if (_data2 == 0)
            {
                _exitcode = DivZero;
            }
            else
            {
                _result = _data1 / _data2;
            }
            break;

        default:
            _exitcode = Unknow;
            break;
        }
    }

    std::string GetRusult()
    {
        std::string r = std::to_string(_data1);
        r += _op;
        r += std::to_string(_data2);
        r += "=";
        r += std::to_string(_result);
        r += "[code: ";
        r += std::to_string(_exitcode);
        r += "]";

        return r;
    }

    std::string GetTask()
    {
        std::string r = std::to_string(_data1);
        r += _op;
        r += std::to_string(_data2);
        r += "= ?";
        return r;
    }

private:
    int _data1;
    int _data2;
    char _op;      // 运算符
    int _result;   // 运算结果
    int _exitcode; // 运算结果是否正确，0表示正确，1表示不正确
};