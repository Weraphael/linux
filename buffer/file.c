#include <stdio.h>
#include <unistd.h>
#include <string.h>

//int main()
//{
//    const char* fstr = "hello fwrite ";
//    const char* cstr = "hello write ";
//
//    printf("hello printf ");
//    fprintf(stdout, "%s", "hello fprintf ");
//    fwrite(fstr, strlen(fstr), 1, stdout);
//    write(1, cstr, strlen(cstr));
//    
//    close(1); // 打印完后将显示器文件关闭
//    return 0;
//}

//int main()
//{
//    const char* fstr = "hello fwrite\n";
//    const char* cstr = "hello write\n";
//
//    printf("hello printf\n");
//    fprintf(stdout, "%s", "hello fprintf\n");
//    fwrite(fstr, strlen(fstr), 1, stdout);
//    write(1, cstr, strlen(cstr));
//    
//    close(1); // 打印完后将显示器文件关闭
//    return 0;
//    
//}


//int main()
//{
//    const char* fstr = "hello fwrite\n";
//    const char* cstr = "hello write\n";
//
//    printf("hello printf\n");
//    fprintf(stdout, "%s", "hello fprintf\n");
//    fwrite(fstr, strlen(fstr), 1, stdout);
//    write(1, cstr, strlen(cstr));
//    
//    fork();
//    return 0;
//}


// 缓冲区的刷新策略
// 1. 无缓冲
//int main()
//{
//    const char* fstr = "hello fwrite";
//    const char* cstr = "hello write";
//
//    printf("hello printf");
//    fprintf(stdout, "%s", "hello fprintf");
//    fwrite(fstr, strlen(fstr), 1, stdout);
//    write(1, cstr, strlen(cstr));
//    
//    fflush(stdout);
//
//    close(1);
//    return 0;
//}

// 2. 行缓冲

//int main()
//{
//    const char* fstr = "hello fwrite\n";
//    const char* cstr = "hello write\n";
//
//    printf("hello printf\n");
//    fprintf(stdout, "%s", "hello fprintf\n");
//    fwrite(fstr, strlen(fstr), 1, stdout);
//    write(1, cstr, strlen(cstr));
//    
//    close(1);
//    return 0;
//}

// 进程退出强制刷新

int main()
{
    printf("hello world");
    return 0;
}

