#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/fcntl.h>

// 以w的方式写入
//int main()
//{
//    // 改变当前进程的工作路径
//    //chdir("/home/wj");
//   
//    // 打开文件
//    // 以w方式打开
//    FILE* fp =  fopen("log.txt", "w");
//    if (fp == NULL)
//    {
//        perror("fopen");
//        return 1;
//    }
//    
//    // 文件写入
//    const char* message = "hello file"; // 待写入的数据
//    size_t fwriteRes = fwrite(message, strlen(message), 1, fp);
//    // size_t fwriteRes = fwrite(message, strlen(message) + 1, 1, fp);
//    if (fwriteRes != 1)
//    {
//        perror("fwrite");
//        return 1;
//    }
//    
//    // 关闭文件
//    fclose(fp);
//    return 0;
//}

// 以a的方式写入
//int main()
//{
//    // 改变当前进程的工作路径
//    //chdir("/home/wj");
//   
//    // 打开文件
//    // 以w方式打开
//    FILE* fp =  fopen("log.txt", "w");
//    if (fp == NULL)
//    {
//        perror("fopen");
//        return 1;
//    }
//    
//    // 文件写入
//    const char* message = "hello file\n"; // 待写入的数据
//    size_t fwriteRes = fwrite(message, sizeof(char), 2, fp);
//    if (fwriteRes != 1)
//    {
//        perror("fwrite");
//        return 1;
//    }
//    
//    // 关闭文件
//    fclose(fp);
//    return 0;
//}

// 文件系统调用 
//
// open函数

//int main()
//{
//    umask(0);
//
//    // 打开文件：以写模式，即O_WRONLY
//    int op = open("log.txt", O_WRONLY | O_CREAT, 0666);
//    if (op == -1)
//    {
//        // 文件打开失败
//        perror("open");        
//    }
//    
//    // 文件写入
//    // ....
//    
//    // 关闭文件
//    close(op);
//    
//    return 0;
//}

// 比特位方式的标志位传递方式

//void show(int flags)
//{
//    if (flags & O_WRONLY)
//    {
//        printf("以只写模式打开文件\n");
//    }
//    if (flags & O_CREAT)
//    {
//        printf("如果文件不存在，则创建文件\n");
//    }
//    if (flags & O_APPEND)
//    {
//        printf("在写入时将数据追加到文件的末尾\n");
//    }
//    // ...
//}
//
//int main()
//{
//    show(O_WRONLY | O_CREAT);
//    printf("=====================================\n");
//    show (O_WRONLY | O_CREAT | O_APPEND);
//
//    return 0;
//}

//int main()
//{
//    // 输出重定向
//    int op = open("log.txt", O_WRONLY | O_CREAT | O_TRUNC, 0666);
//   
//    // 追加重定向
//    // int op = open("log.txt", O_WRONLY | O_CREAT | O_APPEND, 0666);
//    if (op == -1)
//    {
//        perror("open");
//        return 1;
//    }
//
//    const char* message = "hello file\n";
//    ssize_t writeRes = write(op, message, strlen(message));
//    if (writeRes == -1)
//    {
//        perror("write");
//        return 1;
//    }
//    
//    // 关闭文件
//    close(op);
//    return 0;
//}

// 文件描述符

//int main()
//{
//	// 打开文件
//    int op1 = open("log.txt", O_WRONLY | O_CREAT | O_TRUNC, 0666);
//	int op2 = open("log.txt", O_WRONLY | O_CREAT | O_TRUNC, 0666);
//	int op3 = open("log.txt", O_WRONLY | O_CREAT | O_TRUNC, 0666);
//	// 这里我们暂时不对返回值做判断
//	printf("op1: %d\n", op1); //3
//	printf("op2: %d\n", op2); //4
//	printf("op3: %d\n", op3); //5
//	
//	// 写入文件
//	// ...
//
//	// 关闭文件
//	close(op1);
//	close(op2);
//	close(op3);
//	
//	return 0;
//}

// stdin, stdout, stderr
//int main()
//{
//    printf("stdin->%d\n", stdin->_fileno);	
//    printf("stdout->%d\n", stdout->_fileno);	
//    printf("stderr->%d\n", stderr->_fileno);	
//	
//    return 0;
//}

// 引用计数

//int main()
//{
//    close(1);
//    // printf("you can see me\n");
//
//    fprintf(stderr, "%s", "you can see me\n"); 
//    return 0;
//}

// 文件描述符的分配规则

int main()
{
    // 关闭标准输入stdin -> 文件描述符0
    close(0);
    int fd = open("log.txt", O_CREAT | O_WRONLY | O_TRUNC, 0666);// 输出重定向
    if (fd == -1)
    {
        perror("open");
        return 1;
    }
    // 打印文件描述符
    printf("文件描述符：%d\n", fd);
    
    // 文件写入
    const char* message = "hello file\n";
    ssize_t writeRes = write(fd, message, strlen(message));
    if (writeRes == -1)
    {
        perror("writeRes");
        return 1;
    }

    // 关闭文件
    close(fd);
   
    return 0;
}




