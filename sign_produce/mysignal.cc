#include <iostream>
#include <unistd.h>
#include <string>
#include <cstring>
#include <stdlib.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>

using namespace std;

// 验证Ctrl + c是否生成核心转储文件  --- 不生成~
int main()
{
    while (1)
    {
        cout << "hello ctrl C" << endl;
        sleep(1);
    }
    return 0;
}

// 验证利用核心存储文件快速定位
// int main()
// {
//     int a = 1;
//     int b = 0;
//     a = a / b;
//     cout << "a = " << a << endl;

//     return 0;
// }

// 验证dump core码
// int main()
// {
//     pid_t pid = fork();
//     if (pid == 0) // 子进程
//     {
//         int cnt = 500;
//         while (cnt--)
//         {
//             cout << "I am child process, pid:" << getpid() << ", cnt:" << cnt << endl;
//             sleep(1);
//         }
//         // 打印5句话后子进程退出
//         exit(1);
//     }
//     else // 父进程
//     {
//         int status = 0;
//         pid_t rid = waitpid(pid, &status, 0);
//         if (rid == pid)
//         {
//             cout << "child quit info, rid:" << rid << ", exit code:" << ((status >> 8) & 0xff)
//                  << ", exit signal:" << (status & 0x7f) << ", core dump:" << ((status >> 7) & 1)
//                  << endl;
//         }
//     }
//     return 0;
// }

// // alarm函数
// void work()
// {
//     cout << "我在完成其他任务" << endl;
// }

// void handler(int signum)
// {
//     // 异常捕捉时，再设置一个定时器完成其他任务
//     alarm(5);
//     work();
// }

// int main()
// {
//     // 设定一个5s的定时器
//     int x = alarm(5);
//     // 信号捕捉
//     // signal(14, handler);

//     int n = 1;
//     while (true)
//     {
//         // 主要任务
//         cout << x << endl;
//         cout << "我是一个进程，已经运行了 " << n++ << " 秒 PID: " << getpid() << endl;
//         sleep(1);
//     }
//     return 0;
// }

// void handler(int signum)
// {
//     cout << "我收到了" << signum << "号信号" << endl;
// }

// int main()
// {
//     for (int i = 1; i <= 31; i++)
//     {
//         signal(i, handler);
//     }

//     int *p = nullptr;
//     // 野指针
//     *p = 0;

//     return 0;
// }

// void handler(int signum)
// {
//     cout << "我收到了" << signum << "号信号" << endl;
// }

// int main()
// {
//     for (int i = 1; i <= 31; i++)
//     {
//         signal(i, handler);
//     }

//     printf("1 / 0 = %d\n", 1 / 0);
//     return 0;
// }

// void handler(int signum)
// {
//     cout << "我得到了6号信号SIGABRT" << endl;
// }

// int main()
// {
//     // signal(6, handler);
//     //  int cnt = 3;
//     while (true)
//     {
//         cout << "My pid is " << getpid() << endl;
//         sleep(1);
//         // cnt--;
//         // if (cnt == 0)
//         // {
//         //     abort();
//         // }
//     }
//     return 0;
// }

// void handler(int signum)
// {
//     cout << "我得到了2号信号SIGINT" << endl;
//     exit(1);
// }

// // raise函数
// int main()
// {
//     signal(2, handler);
//     int cnt = 3;
//     while (true)
//     {
//         cout << "My pid is " << getpid() << endl;
//         sleep(1);
//         cnt--;
//         if (cnt == 0)
//         {
//             // 2号信号
//             raise(SIGINT);
//         }
//     }
//     return 0;
// }

// void Usage(string proc)
// {
//     cout << "格式:\n\t" << proc << " signum pid\n\n";
// }

// // argv：程序名argv[0]、信号编号argv[1]、进程pid argv[2]
// // 系统调用kill
// int main(int argc, char *argv[])
// {
//     // 如果是三个参数,可以杀掉用户指定进程
//     if (argc == 3)
//     {

//         int signum = stoi(argv[1]);
//         pid_t pid = stoi(argv[2]);

//         int n = kill(pid, signum);
//         if (n != 0)
//         {
//             perror("kill");
//             exit(2);
//         }
//     }
//     // 如果不是输入三个参数，就提醒用户输入格式
//     else
//     {
//         Usage(argv[0]);
//         exit(1);
//     }

//     return 0;
// }

// void myhandler(int signum)
// {
//     cout << "信号编号为：" << signum << endl;
//     exit(1);
// }

// Ctrl + \
// int main()
// {
//     signal(SIGQUIT, myhandler);
//     while (true)
//     {
//         cout << "我是一个进程，我在做死循环操作" << endl;
//         sleep(1);
//     }
//     return 0;
// }

// Ctrl + c
// int main()
// {
//     signal(SIGINT, myhandler);
//     while (true)
//     {
//         cout << "我是一个进程，我在做死循环操作" << endl;
//         sleep(1);
//     }
//     return 0;
// }