#include <iostream>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>

using namespace std;

const char *pathname = "/home/wj";
int proj_id = 'A';

int main()
{
    // 使用ftok函数生成键值
    key_t key = ftok(pathname, proj_id);
    printf("key is 0x%x\n", key);

    // 创建消息队列
    int msqid = msgget(key, IPC_CREAT | IPC_EXCL | 0666);
    printf("msqid is %d\n", msqid);

    // 进程结束前释放消息队列
    // msgctl(msqid, IPC_RMID, nullptr);

    return 0;
}