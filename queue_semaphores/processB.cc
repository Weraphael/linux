#include <iostream>
#include <sys/ipc.h>
#include <sys/types.h>
#include <sys/shm.h>
#include <string>
#include <cstring>

using namespace std;

const string pathname = "/home/wj"; // ftok函数的第一个参数
int proj_id = 'C';                  // ftok函数的第二个参数

int main()
{
    int key = ftok(pathname.c_str(), proj_id);
    int shmid = shmget(key, 4096, IPC_CREAT | IPC_EXCL | 0666);

    return 0;
}