#include <iostream>
#include <pthread.h> // Linux下的线程库
#include <unistd.h>
#include <string>
// #include<thread> // C++下的线程库
using namespace std;

void thread_task()
{
    while (true)
    {
        cout << "I am a new thread for C++" << endl;
        sleep(1);
    }
}

int main()
{
    thread t1(thread_task);

    t1.join();

    return 0;
}

// 线程分离
// void *thread_task(void *)
// {
//     int cnt = 3;
//     while (cnt--)
//     {
//         printf("Thread started\n");
//         sleep(1);
//     }

//     printf("Thread exiting\n");
//     pthread_exit((void *)233);
// }

// int main()
// {
//     pthread_t thread;

//     // 创建线程
//     pthread_create(&thread, nullptr, thread_task, nullptr);

//     // 将线程设置为分离状态
//     pthread_detach(thread);

//     // 主线程继续执行其他操作
//     printf("Main thread continuing...\n");

//     // 主线程可能继续执行其他操作，而不必等待分离状态的线程结束
//     int cnt = 8;
//     while (cnt--)
//     {
//         cout << "主线程可能继续执行其他操作，而不必等待分离状态的线程结束" << endl;
//         sleep(1);
//     }
//     return 0;
// }

// // 计算
// class Request
// {
// public:
//     Request(int start, int end, const string &threadname)
//         : _start(start), _end(end), _threadname(threadname)
//     {
//     }

// public:
//     int _start;
//     int _end;
//     string _threadname;
// };

// // 计算结果
// class Responese
// {
// public:
//     Responese(int result, int exitcode)
//         : _result(result), _exitcode(exitcode)
//     {
//     }

// public:
//     int _result;   // 计算结果
//     int _exitcode; // 计算结果是否可靠
// };

// void *sumCount(void *arg)
// {
//     Request *rq = static_cast<Request *>(arg);
//     Responese *rsp = new Responese(0, 0);
//     for (int i = rq->_start; i <= rq->_end; i++)
//     {
//         // 运算过程
//         cout << rq->_threadname << " is running, cacling..., " << i << endl;
//         usleep(100000);
//         rsp->_result += i;
//     }
//     delete rq;
//     return (void *)rsp;
// }

// 线程可以传对象
// int main()
// {
//     pthread_t tid;
//     // 求1~100总和
//     Request *rq = new Request(1, 100, "thread 1");
//     // 创建线程
//     pthread_create(&tid, nullptr, sumCount, rq); // 传对象

//     void *res;
//     pthread_join(tid, &res);
//     Responese *rsp = static_cast<Responese *>(res);
//     cout << "rsp->_result = " << rsp->_result << " , exitcode: " << rsp->_exitcode << endl;

//     delete rsp;
//     return 0;
// }

// const char *symbol = "# ";

// void sayhello(const string &words)
// {
//     cout << "你好, 我的名字是" << symbol << words << endl;
// }

// // 新线程
// void *thread_task(void *arg)
// {
//     int cnt = 3;
//     while (cnt--)
//     {
//         sayhello((char *)arg);
//         sleep(1);
//     }
//     // // 当cnt=0走到这里，新线程就退出了。
//     // return (void *)233;
//     // exit(233); // exit是终止进程的
//     pthread_exit((void *)233);
// }

// 一些常见接口学习
// int main()
// {
//     pthread_t tid;
//     char *name = new char[32];
//     snprintf(name, 32, "thread 1");

//     // 创建线程
//     pthread_create(&tid, nullptr, thread_task, name);
//     sleep(1); // 保证线程已启动

//     // 线程取消
//     pthread_cancel(tid);

//     void *res;
//     // 线程等待
//     pthread_join(tid, &res);
//     cout << "次线程退出了，其退出信息为： " << (long long)res << endl;

//     // // 主线程
//     // while (true)
//     // {
//     //     sayhello("主线程");
//     //     sleep(1);
//     // }

//     return 0;
// }
