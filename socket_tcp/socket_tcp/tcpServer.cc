#include <iostream>
#include "tcpServer.hpp"
#include <memory>
#include <string>
using namespace std;

// ./xxx 8888
void Usage(const string &proc)
{
    cout << "\n\tUsage: " << proc << " port(1024+)" << endl
         << endl;
}

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        Usage(argv[0]);
        exit(1);
    }

    uint16_t port = stoi(argv[1]);

    // 1. 创建TCP服务器端对象
    unique_ptr<tcpserver> tcpsvr(new tcpserver(port));
    // 2. 初始化TCP服务器
    tcpsvr->Init();
    // 3. 启动TCP服务器
    tcpsvr->Run();

    return 0;
}