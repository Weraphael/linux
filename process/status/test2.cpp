#include <stdlib.h>
#include <string>
#include <iostream>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
using namespace std;

// ===== 运行状态 ======
//int main() 
//{
//    int i = 0;
//    while (1)
//    {
//        ++i;
//    }
//
//    return 0;
//}

// ===== 浅度睡眠状态 ======    
//int main()
//{
//    string s;
//    printf("Input:");
//    getline(cin, s);
//
//    printf("Output:");
//    cout << s << endl;
//    return 0;
//}


// ===== 停止状态 ======
//int main()
//{
//    while (1)
//    {
//        cout << "hello process" << endl;
//    }
//    return 0;
//}

// =====  追踪状态 ======
//int main()
//{
//    printf("hello process\n");
//    printf("hello process\n");
//    printf("hello process\n");
//    printf("hello process\n");
//    printf("hello process\n");
//    printf("hello process\n");
//    printf("hello process\n");
//    printf("hello process\n");
//    printf("hello process\n");
//    printf("hello process\n");
//    printf("hello process\n");
//    printf("hello process\n");
//    printf("hello process\n");
//
//    return 0;
//}

// ======== 僵尸进程 ===========
//int main()
//{
//    pid_t pid  = fork();
//
//    if (pid > 0)
//    {
//        // 父进程
//        while (1)
//        {
//            cout << "我是父进程" << endl;
//        }
//        
//    }
//    else if (pid == 0)
//    {
//        // 子进程
//        
//        exit(0);
//    }
//    else 
//    {
//        // 子进程创建失败
//        cout << "子进程创建失败" << endl;
//    }
//    return 0;
//}

// ======  孤儿进程 =======
int main()
{
    pid_t pid = fork();

    if (pid > 0)
    {
        // 父进程
        int cnt = 3;
        while(cnt--)
        {
            printf("I am father, my PID is %d, my PPID is %d, my count is %d\n",getpid(), getppid(),cnt);
            sleep(1);
        }
        exit(0);
    }
    else if (pid == 0)
    {
        // 子进程
        while (1)
        {
            printf("I am child, my PID is %d, my PPID is %d\n",getpid(), getppid());
            sleep(1);
        }
    }
    else 
    {
        printf("fork erro\n");
    }
    return 0;
}
