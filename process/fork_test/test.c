#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>

int main()
{

    printf("bash的进程id:%d\n", getpid());
    // 创建子进程
    pid_t pid = fork();

    if (pid > 0)
    {
        // 父进程
        printf("This is the parent process. My pid is %d and my child's PID is %d\n", getpid(), pid);
    }
    else if (pid == 0)
    {
        // 子进程
        printf("This is the child process. My PID is %d\n", getpid());
    }
    else 
    {
        printf("创建子进程失败\n");
    }




   // printf("111\n");
   // // 创建进程
   // fork();
   // printf("222\n");

   // return 0;
}
