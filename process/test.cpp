#include <iostream>
#include <unistd.h>
#include <sys/types.h>
using namespace std;

int main()
{
    while (1)
    {
        cout << "I am a process, my pid is " << getpid() << ", myppid is " << getppid() << endl;
        sleep(1);
    }
    return 0;
}
