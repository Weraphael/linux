#include <stdio.h>
#include <unistd.h>
#include <iostream>
using namespace std;

// 批量化操作
// 1. ctrl + v后选择你需要的代码块
// 2. shift + i 输入vim的命令指令或者你需要添加的批量化内容
// 3. 最后按2下esc即可

// 倒计时为9
//int main()
//{
//    int cnt = 9;
//
//    while (cnt >= 0)
//    {
//        // 显示完一个数字后停顿1秒
//        printf("%d\r", cnt);
//        fflush(stdout);
//
//        sleep(1);
//        cnt--;
//    }
//    return 0;
//}

// 倒计时为10

int main()
{
    int cnt = 10;
    while (cnt >= 0)
    {
        printf("%-2d\r",cnt--);
        fflush(stdout);

        sleep(1);
    }
   printf("你输了\n");
    return 0;
}
