#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>

// 验证进程空间地址分布

int gal_a = 100;
int gal_b;
int main(int argc, char *argv[], char *env[])
{
    printf("代码区: %p\n", main);

    const char* c = "hello world";
    printf("字符常量区: %p\n", c);

    printf("已初始化全局变量: %p\n", &gal_a);
   
    printf("未初始化全局变量: %p\n", &gal_b);

    char* mem1 = (char*)malloc(sizeof(char) * 10);
    char* mem2 = (char*)malloc(sizeof(char) * 10);
    char* mem3 = (char*)malloc(sizeof(char) * 10);
    
    printf("堆区: %p\n", mem1);
    printf("堆区: %p\n", mem2);
    printf("堆区: %p\n", mem3);

    
    printf("栈区: %p\n", &mem1);
    printf("栈区: %p\n", &mem2);
    printf("栈区: %p\n", &mem3);
    
    int i = 0;
    for (; argv[i]; i++)
    {
        printf("argv[%d]: %p\n",i, argv[i]);
    }
    i = 0;
    for (; env[i]; i++)
    {
        printf("env[%d]: %p\n",i, env[i]);
    }

    static int a = 10;
    printf("static int a = %p\n", &a);
    return 0;
}

// 验证写时拷贝
//int main()
//{
//    pid_t pid = fork();
//    
//    int data = 0;
//
//    if (pid == 0)
//    {
//        // 子进程
//        while (1)
//        {
//            printf("I am son. My ppid is %d. My pid is %d. My data is %d -> %p\n", getppid(), getpid(), data, &data);
//            sleep(1);
//            data = 985;
//        }
//    }
//    else if (pid > 0)
//    {
//        // 父进程
//        while (1)
//        {
//             printf("I am father. My pid is %d. My data is %d -> %p\n", getpid(), data, &data);
//             sleep(1);
//        }
//    }
//    else 
//    {
//        // 子进程创建失败
//        printf("子进程创建失败\n");
//    }
//    return 0;
//}
