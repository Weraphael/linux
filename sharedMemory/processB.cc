#include "resource.hpp"

// ===========================  简易通讯 ===================================
// #include "resource.hpp"

// int main()
// {
//     // 1. 获取shmid
//     int shmid = GetShm();

//     // 2. 挂接共享内存
//     char *shmaddress = (char *)shmat(shmid, nullptr, 0);

//     // 3. 通信
//     while (true)
//     {
//         // processB进程作为服务端，负责写
//         cout << "请输入：";
//         char *context = fgets(shmaddress, 4096, stdin);
//         if (context != nullptr && strcmp(shmaddress, "quit\n") == 0)
//         {
//             break;
//         }
//     }
//     // 4. 取消挂接
//     shmdt(shmaddress);

//     return 0;
// }

// ======================= 解决不同步互斥问题 ========================

int main()
{
    // 1. 获取shmid
    int shmid = GetShm();

    // 2. 挂接共享内存
    char *shmaddress = (char *)shmat(shmid, nullptr, 0);

    // 打开命名管道
    int fd = open("./myfifo", O_WRONLY);
    if (fd == -1)
    {
        exit(OPEN_FAIL);
    }

    // 3. 通信
    while (true)
    {
        // processB进程作为服务端，负责写
        cout << "请输入：";
        char *context = fgets(shmaddress, 4096, stdin);
        // 写完之后，通知对方来读取。
        write(fd, "c", 1);
        if (context != nullptr && strcmp(shmaddress, "quit\n") == 0)
        {
            break;
        }
    }
    // 4. 取消挂接
    shmdt(shmaddress);
    close(fd);

    return 0;
}
