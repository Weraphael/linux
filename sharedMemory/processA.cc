#include "resource.hpp"

// ====================== 系统调用接口测试 ==========================

// int main()
// {
//     // 1. 创建共享内存段
//     int shmid = GetShareMem();
//     l.logmessage(Debug, "create shm done");

//     // 2. 挂接共享内存段
//     char *shmaddress = (char *)shmat(shmid, nullptr, 0);
//     if (shmaddress == nullptr)
//     {
//         l.logmessage(Error, "processA attach failed, shmid is %d", shmid);
//     }
//     l.logmessage(Debug, "processA attach success, shmid is %d", shmid);

//     // 3. 销毁共享内存段
//     int res = shmctl(shmid, IPC_RMID, nullptr);
//     if (res == -1)
//     {
//         l.logmessage(Error, "share Memory destroy failed, shmid is %d", shmid);
//         exit(Error);
//     }
//     l.logmessage(Debug, "share Memory destroy success, shmid is %d", shmid);

//     l.logmessage(Debug, "process quit...");
//     return 0;
// }

// =======================   简化版通信   ========================================

// int main()
// {
//     // 1. 创建共享内存段
//     int shmid = GetShareMem();

//     // 2. 挂接共享内存段
//     char *shmaddress = (char *)shmat(shmid, nullptr, 0);

//     // 3. 通信
//     while (true)
//     {
//         // 假设processA进程作为客户端，负责读取
//         cout << "client say@ " << shmaddress << endl;
//         if (strcmp(shmaddress, "quit\n") == 0)
//         {
//             break;
//         }
//         sleep(1);
//     }

//     // 4. 取消挂接
//     shmdt(shmaddress);

//     // 5. 销毁共享内存段
//     int res = shmctl(shmid, IPC_RMID, nullptr);

//     return 0;
// }

// ======================= 查看共享内存属性 ========================

// int main()
// {
//     //  创建共享内存段
//     int shmid = GetShareMem();

//     // 挂接共享内存段
//     char *shmaddress = (char *)shmat(shmid, nullptr, 0);

//     // 获取共享内存的属性
//     struct shmid_ds shmds;
//     shmctl(shmid, IPC_STAT, &shmds);
//     cout << "共享内存的大小：" << shmds.shm_segsz << endl;
//     cout << "共享内存的连接数：" << shmds.shm_nattch << endl;
//     printf("共享内存的key值：0x%x\n", shmds.shm_perm.__key);

//     // 取消挂接
//     shmdt(shmaddress);

//     return 0;
// }

// ======================= 解决不同步互斥问题 ========================
int main()
{
    // 创建管道
    Init init;

    // 1. 创建共享内存段
    int shmid = GetShareMem();

    // 2. 挂接共享内存段
    char *shmaddress = (char *)shmat(shmid, nullptr, 0);

    // 打开命名管道
    int fd = open("./myfifo", O_RDONLY);
    if (fd == -1)
    {
        exit(OPEN_FAIL);
    }

    // 3. 通信
    while (true)
    {
        // 假设processA进程作为客户端，负责读取
        // 在读取之前先去管道看看是否有通知
        char c;
        ssize_t s = read(fd, &c, 1);
        // s == 0 说明没读到，那就继续读取
        if (s == 0)
        {
            continue;
        }
        // s == -1说明读取发生错误，那就退出
        if (s == -1)
        {
            break;
        }

        cout << "client say@ " << shmaddress << endl;
        if (strcmp(shmaddress, "quit\n") == 0)
        {
            break;
        }
        sleep(1);
    }

    // 4. 取消挂接
    shmdt(shmaddress);

    // 5. 销毁共享内存段
    int res = shmctl(shmid, IPC_RMID, nullptr);
    close(fd);

    return 0;
}