#define _CRT_SECURE_NO_WARNINGS 1

#include <WinSock2.h>
#include <winsock.h>
#include <iostream>
#include<string>
#include <cstring>
using namespace std;

/*
是一个 Microsoft Visual C++ 特有的指令，用于在编译时自动链接到 ws2_32.lib 库。
这个库提供了 Winsock 2 的支持，用于网络编程。通过使用这个指令，你不需要在项目设置
中手动添加库，编译器会自动处理链接过程
*/
#pragma comment(lib, "ws2_32.lib")

/*

#pragma warning(disable:4996) 是一个 Microsoft Visual C++ 特有的指令，用于禁用编译器的特定警告，
警告代码 4996 通常涉及使用不安全的函数，比如 strcpy、sprintf和inet_addr等。
这个指令可以帮助你避免这些警告，但需要注意，禁用警告可能会掩盖潜在的安全问题。
*/
#pragma warning(disable:4996) 


string server_ip = "175.178.46.38"; 
uint16_t server_port = 8080;
int main()
{
	cout << "Hello client" << endl;

    // 在Windows中，进行任何网络操作（如创建套接字、发送和接收数据）前的必要准备
    // 是要初始化Winsock 库

    // 初始化 Winsock 库。
	WSADATA wsd;
    // 指定需要使用的 Winsock 版本（2.2），并将 wsd 传递给它以获取初始化信息。如果调用成功，Winsock 将被初始化并准备好进行网络操作
	WSAStartup(MAKEWORD(2, 2), &wsd);

	// 直接复制 客户端代码
    struct sockaddr_in server;
    memset(&server,0, sizeof(server));
    server.sin_family = AF_INET;
    server.sin_port = htons(server_port);
    server.sin_addr.s_addr = inet_addr(server_ip.c_str());
    

    // 创建套接字
    SOCKET sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if (sockfd == SOCKET_ERROR)
    {
        cout << "client socket create error" << endl;
        return 1;
    }

    string message;
    char buffer[1024];
    while (true)
    {
        cout << "Please Enter@ ";
        getline(cin, message);

        sendto(sockfd, message.c_str(), (int)message.size(), 0, (struct sockaddr*)&server, (int)sizeof(server));

        struct sockaddr_in temp;
        int len = sizeof(temp);
        int s = recvfrom(sockfd, buffer, sizeof(buffer) - 1, 0, (struct sockaddr*)&temp, &len);

        if (s > 0)
        {
            buffer[s] = 0;
            cout << buffer << endl;
        }
    }
    // 关闭套接字文件
    closesocket(sockfd);
    
    // 在完成所有网络操作后被调用，以释放 WSAStartup 初始化时所分配的资源和清理 Winsock 库的状态。
    WSACleanup();

	return 0;
}