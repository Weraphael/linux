#include <signal.h>
#include <unistd.h>
#include <iostream>
#include <sys/types.h>
#include <sys/wait.h>
using namespace std;

void handler(int signum)
{
    pid_t rid;
    while ((waitpid(-1, nullptr, WNOHANG)) > 0)
    {
        cout << "who am I: " << getpid()
             << ", catch a signum: " << signum
             << ", 子进程" << rid << "回收成功!" << endl;
    }
}

int main()
{
    signal(17, handler);

    for (int i = 0; i < 10; i++)
    {
        pid_t id = fork();
        if (id == 0) // 子进程
        {
            while (true)
            {
                cout << "I am child: " << getpid() << ", ppid: " << getppid() << endl;
                sleep(5); //
                break;
            }
            cout << "子进程退出..." << endl;
            exit(17); // 打印一句后的5秒，子进程直接退出
        }
    }
    // 父进程
    while (true) // 父进程不能退出
    {
        cout << "I am father: " << getpid() << endl;
        sleep(1);
    }
    return 0;
}

// volatile int flag = 0;

// void handler(int signum)
// {
//     cout << "捕捉" << signum << "号信号" << endl;
//     flag = 1;
// }

// volatile 关键字
// int main()
// {
//     signal(2, handler);
//     while (!flag)
//     {
//         ;
//     }

//     cout << "进程正常退出" << endl;
//     return 0;
// }