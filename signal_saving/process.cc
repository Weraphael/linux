#include <iostream>
#include <signal.h>
#include <unistd.h>
using namespace std;

int main()
{
    // 获取进程pid。方便使用kill命令
    cout << "my pid is " << getpid() << endl;
    sleep(3);

    sigset_t sset, oldset;
    sigemptyset(&sset);
    sigemptyset(&oldset);
    for (int i = 1; i <= 31; i++)
    {
        sigaddset(&sset, i);
    }
    // 将所有的信号屏蔽
    sigprocmask(SIG_SETMASK, &sset, &oldset);
    sigset_t pending_t;
    while (true)
    {
        int n = sigpending(&pending_t);
        if (n < 0)
            continue;
        for (int signo = 31; signo >= 1; signo--)
        {
            cout << sigismember(&pending_t, signo);
        }
        cout << endl
             << endl;
        sleep(1);
    }

    return 0;
}

// void handler(int signum)
// {
//     cout << "解除" << signum << "号信号的阻塞 " << endl;
//     // 最终不退出进程 没加exit
// }

// int main()
// {
//     // 捕捉2号信号
//     signal(2, handler);

//     // 在用户层定义sigset_t变量
//     sigset_t sset;
//     // 初始化信号集，将所有信号的对应比特位清零
//     sigemptyset(&sset);
//     // 将信号集sset添加特定的信号，设置1
//     sigaddset(&sset, 2);
//     // sigprocmask函数用来对block表进行操作（阻塞信号）
//     sigset_t oldset;
//     sigemptyset(&oldset);
//     sigprocmask(SIG_SETMASK, &sset, &oldset);

//     // 重复打印当前进程的pending表。
//     // 虽然我们阻塞了2号信号，但是只有没产生信号，pending表一定是全0
//     sigset_t pending_t; // 获取pending表
//     int cnt = 0;
//     while (true)
//     {
//         // sigpending函数用来获取当前进程中的未决信号集pending表
//         int n = sigpending(&pending_t);
//         if (n < 0)
//             continue;
//         // 打印
//         // 判断当前信号集中是否存在该信号，如果存在，输出1，否则输出0
//         for (int i = 31; i >= 1; i--)
//         {
//             if (sigismember(&pending_t, i))
//             {
//                 cout << "1";
//             }
//             else
//             {
//                 cout << "0";
//             }
//         }
//         cout << endl
//              << endl;

//         sleep(1);
//         // 解除阻塞
//         cnt++;
//         if (cnt == 4)
//         {
//             sigprocmask(SIG_SETMASK, &oldset, nullptr);
//         }
//     }
//     return 0;
// }

// int main()
// {
//     sigset_t
//         // 忽略信号处理动作
//         // 用户使用组合键ctrl + c可触发2号信号
//         // signal(2, SIG_IGN);

//         // 采用系统默认处理动作
//         signal(2, SIG_DFL);
//     while (true)
//     {
//         cout << "Ctrl + c是干不掉我的hh" << endl;
//         sleep(1);
//     }
//     return 0;
// }
