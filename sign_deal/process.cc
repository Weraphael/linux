#include <iostream>
#include <signal.h>
#include <cstring>
#include <unistd.h>
using namespace std;

void DisplayPending(sigset_t pending)
{
    // 打印 pending 表
    cout << "当前进程的 pending 表为: ";
    int i = 1;
    while (i < 32)
    {
        if (sigismember(&pending, i))
            cout << "1";
        else
            cout << "0";

        i++;
    }
    cout << endl;
}

// 自定义信号处理函数
// void handler(int signo)
// {
//     cout << "捕捉到了一个信号：" << signo << endl;
//     int n = 12;
//     while (n--)
//     {
//         // 获取进程的pending表
//         sigset_t pending;
//         sigemptyset(&pending);

//         int n = sigpending(&pending);
//         if (n < 0)
//         {
//             continue;
//         }

//         DisplayPending(pending);
//         sleep(1);
//     }
//     exit(0);
// }

// sigaction函数的基本使用
// int main()
// {
//     struct sigaction act, oldact;

//     // 初始化结构体对象（可选）
//     memset(&act, 0, sizeof(act));
//     memset(&oldact, 0, sizeof(oldact));

//     // 设置信号处理函数
//     act.sa_handler = handler;
//     // 初始化 屏蔽信号集
//     sigaddset(&act.sa_mask, 3);
//     sigaddset(&act.sa_mask, 4);
//     sigaddset(&act.sa_mask, 5);

//     //  给2号信号注册自定义动作
//     sigaction(2, &act, &oldact);

//     while (true)
//     {
//         cout << "I am a process: " << getpid() << endl;
//         sleep(1);
//     }

//     return 0;
// }

// 修改成了死循环而已 ~
void handler(int signo)
{
    cout << "捕捉到了一个信号：" << signo << endl;
    while (true)
    {
        // 获取进程的pending表
        sigset_t pending;
        sigemptyset(&pending);

        int n = sigpending(&pending);
        if (n < 0)
        {
            continue;
        }

        DisplayPending(pending);
        sleep(1);
    }
    exit(0);
}
// 当某个信号的处理函数被调用时，内核自动会将当前信号加入进程的信号屏蔽字，也就是加入到pending表中
int main()
{
    struct sigaction act, oldact;

    // 初始化结构体对象（可选）
    memset(&act, 0, sizeof(act));
    memset(&oldact, 0, sizeof(oldact));

    // 设置信号处理函数
    act.sa_handler = handler;

    //  给2号信号注册自定义动作
    sigaction(2, &act, &oldact);

    while (true)
    {
        cout << "I am a process: " << getpid() << endl;
        sleep(1);
    }

    return 0;
}
