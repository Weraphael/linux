#include <string.h>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
using namespace std;

//int main()
//{
//    // 使用系统调用接口获取环境变量value
//    cout << "PATH=" << getenv("PATH") << endl;
//    return 0;
//}

// 命令行参数
// 查看字符指针数组argv有什么
// 存储的是命令行参数，包括程序名本身
//int main(int argc, char* argv[])
//{
//    int i = 0;
//    // 最后一个字符指针为空
//    for (; argv[i]; i++)
//    {
//        printf("argv[%d]->%s\n", i, argv[i]);
//    }
//    return 0;
//}

// 命令行参数的用途：指定程序的行为

//int main(int argc, char* argv[])
//{
//    if (argc != 2)
//    {
//        printf("请选择:1. 开始 2. 退出\n");
//        return 1;
//    }
//
//    if (strcmp(argv[1], "1") == 0) 
//    {
//        cout << "游戏开始" << endl;
//    }
//
//    else  if (strcmp(argv[1], "2") == 0) 
//    {
//        cout << "退出游戏" << endl;
//    }
//
//    else 
//    {
//        cout << "输入错误，请重新输入" << endl;
//    }
//    return 0;
//}

// main函数的第三个参数：字符指针数组env -> 获取当前进程的环境变量

//int main(int argc, char* argv[], char* env[])
//{
//    int i = 0;
//    for (; env[i]; i++)
//    {
//        printf("env[%d]->%s\n", i, env[i]);
//    }
//    return 0;
//}

int main(int argc, char* argv[])
{
    // 通过第三方变量environ获取
    extern char **environ;
    int i = 0;
    for (; environ[i]; i++)
    {    
        printf("env[%d]->%s\n", i, environ[i]);
    }
    return 0;
}
