#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>

// 输出重定向
//int main()
//{
//    // 关闭stdout文件
//    close(1);
//    // 打开文件
//    int fd = open("log.txt", O_WRONLY | O_CREAT | O_TRUNC, 0666);
//    if (fd == -1)
//    {
//        perror("open");
//        return 1;
//    }
//    
//
//    // 文件写入
//    const char* message = "hello file\n";
//    // 向stdout文件写入，其文件描述符为1，本质就是向显示器文件写入
//    ssize_t writeRes = write(1, message, strlen(message));
//    if (writeRes == -1)
//    {
//        perror("write");
//        return 1;
//    }
//    
//    // 关闭文件
//    close(fd);
//    
//    return 0;
//}

// 追加重定向
//int main()
//{
//    // 关闭stdout文件
//    close(1);
//    // 打开文件
//    int fd = open("log.txt", O_WRONLY | O_CREAT | O_APPEND ,0666);
//    if (fd == -1)
//    {
//        perror("open");
//        return 1;
//    }
//
//    // 文件写入
//    const char* message = "hello file\n";
//    // 向stdout文件写入，其文件描述符为1，本质就是向显示器文件写入
//    ssize_t writeRes = write(1, message, strlen(message));
//    if (writeRes == -1)
//    {
//        perror("write");
//        return 1;
//    }
//    
//    // 关闭文件
//    close(fd);
//    
//    return 0;
//}

// 输入重定向

//int main()
//{
//    // 本应该从标准输入stdin中读取数据的方式改变为从其他文件读取数据
//    close(0);
//    int fd = open("log.txt", O_RDONLY); // 打开方式以只读
//    if (fd == -1)
//    {
//        perror("open");
//        return 1;
//    }
//
//    // 读文件
//    char text[1024];
//    memset(text, '\0', sizeof text);
//    ssize_t readRes = read(0, text, sizeof(text));
//    if (readRes == -1)
//    {
//        perror("read");
//        return 1;
//    }
//    else 
//    {
//        //text[readRes] = '\0';
//        printf("%s\n", text);
//    }
//
//    // 关闭文件
//    close(fd);
//    
//    return 0;
//}

// 系统调用接口dup2的使用

//int main()
//{
//    // 打开文件
//    int fd = open("log.txt", O_WRONLY | O_CREAT | O_APPEND, 0666); // 追加重定向
//    if (fd == -1)
//    {
//        // 打开文件失败
//        perror("open");
//        return 1;
//    }
//
//    // 文件写入
//    dup2(fd, 1);
//    const char* message = "hello file\n";
//    ssize_t writeRes = write(1, message, strlen(message));
//    if (writeRes == -1)
//    {
//        // 文件写入失败
//        perror("write");
//        return 1;
//    }
//    // 关闭文件
//    close(fd);
//    return 0;
//}


//int main()
//{
//    // 打开文件
//    int fd = open("log.txt", O_WRONLY | O_CREAT | O_TRUNC, 0666); // 输出重定向
//    if (fd == -1)
//    {
//        // 打开文件失败
//        perror("open");
//        return 1;
//    }
//
//    // 文件写入
//    dup2(fd, 1);
//    const char* message = "hello file\n";
//    ssize_t writeRes = write(1, message, strlen(message));
//    if (writeRes == -1)
//    {
//        // 文件写入失败
//        perror("write");
//        return 1;
//    }
//    // 关闭文件
//    close(fd);
//    return 0;
//}


//int main()
//{
//    // 打开文件
//    int fd = open("log.txt", O_RDONLY); // 输入重定向
//    if (fd == -1)
//    {
//        // 打开文件失败
//        perror("open");
//        return 1;
//    }
//
//    // 文件读取
//    dup2(fd, 0); // 标准输入中读取数据的方式改变为从其他文件读取数据
// 
//    char ans[1024];
//    memset(ans, '\0', sizeof(ans));
//    ssize_t readRes = read(0, ans, sizeof(ans));
//    if (readRes == -1)
//    {
//        perror("read");
//        return 1;
//    }
//    else 
//    {
//        printf("%s", ans);
//    }
//    // 关闭文件
//    close(fd);
//    return 0;
//}

// stdout 和 stdin 的区别
int main()
{
    fprintf(stdout, "%s\n", "I am stdout");
    fprintf(stdout, "%s\n", "I am stdout");
    fprintf(stdout, "%s\n", "I am stdout");

    fprintf(stderr, "%s\n", "I am stderr");
    fprintf(stderr, "%s\n", "I am stderr");
    fprintf(stderr, "%s\n", "I am stderr");

    return 0;
}
