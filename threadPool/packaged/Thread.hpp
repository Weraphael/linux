#pragma once

#include <iostream>
#include <string>
#include <pthread.h>
#include <ctime>
#include <functional>

// 参数、返回值为 void 的函数类型
typedef void (*callback_t)();

class Thread
{
    static int num;

public:
    static void *Routine(void *args)
    {
        Thread *thread = static_cast<Thread *>(args);
        thread->Entery();

        return nullptr;
    }

public:
    Thread(callback_t cb = nullptr)
        : _tid(0), _threadname(""), _Start_timestamp(0), _status(false), _cb(cb)
    {
    }

    ~Thread()
    {
    }

    // 启动线程
    void Run()
    {
        _threadname = "thread-" + std::to_string(num++);
        _Start_timestamp = time(nullptr);
        _status = true;
        pthread_create(&_tid, nullptr, Routine, this);
    }

    // 线程等待
    void Join()
    {
        pthread_join(_tid, nullptr);
        _status = false;
    }

    // 获取线程名
    std::string GetName()
    {
        return _threadname;
    }

    // 获取线程启动的时间戳
    uint64_t GetStartTime()
    {
        return _Start_timestamp;
    }

    // 获取线程状态
    bool Status()
    {
        return _status;
    }

    void Entery()
    {
        _cb();
    }

private:
    pthread_t _tid;            // 线程tid
    std::string _threadname;   // 线程名字
    uint16_t _Start_timestamp; // 时间戳(线程什么时候启动的)
    bool _status;              // 线程状态
    callback_t _cb;            // 线程回调函数(表示线程要执行的任务)
};
// 静态成员变量初始化需要在类外
int Thread::num = 1;