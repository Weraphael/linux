#include <iostream>
#include "Thread.hpp"
#include <unistd.h>
using namespace std;

void Print()
{
    while (true)
    {
        cout << "我是一个封装的线程" << endl;
        sleep(1);
    }
}

int main()
{
    Thread t(Print); // 创建线程
    t.Run();         // 启动线程

    /* 查看线程属性 */
    cout << "是否启动成功: " << t.Status() << endl;
    cout << "启动成功的时间戳: " << t.GetStartTime() << endl;
    cout << "线程的名字: " << t.GetName() << endl;

    t.Join(); // 线程等待
    return 0;
}

// ================= 使用C++11线程库 =======================
// #include <iostream>
// #include <unistd.h>
// #include <thread> // C++11线程库的头文件

// using namespace std;

// void run()
// {
//     while (true)
//     {
//         cout << "我是一个封装的线程" << endl;
//         sleep(1);
//     }
// }

// int main()
// {
//     thread t(run); // 创建了一个新的线程 t，并且启动它来执行run函数中的代码
//     t.join();      // 线程等待
//     return 0;
// }
