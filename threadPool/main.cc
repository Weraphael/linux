#include "ThreadPool.hpp"
#include "Task.hpp"
#include <ctime>

using namespace std;

int main()
{
    srand(time(nullptr));

    ThreadPool<Task> *tp = new ThreadPool<Task>(5); // 表示线程此有5个线程
    tp->start();

    while (true)
    {
        // 1. 构建任务
        int x = rand() % 10 + 1;
        usleep(10); // 防止x和y的值类似
        int y = rand() % 5;

        int len = oper.size();
        char op = oper[rand() % len];

        Task t(x, y, op);
        // 2.  交给线程池处理
        tp->push(t);
        cout << "main thread make a task: " << t.GetTask() << endl;

        sleep(1);
    }
    return 0;
}