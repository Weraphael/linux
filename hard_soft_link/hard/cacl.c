#include "cacl.h"

int Add(int x, int y)
{
    return x + y;
}

int Sub(int x, int y)
{
    return x - y;
}

int Mul(int x, int y)
{
    return x * y;
}

int Div(int x, int y)
{
    if (y == 0){
        printf("除0错误\n");
        return -1;
    }
    return x / y;
}
