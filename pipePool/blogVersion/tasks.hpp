#include <iostream>
#include <vector>
#include <functional>

std::vector<std::function<void()>> taskList; // 任务列表

void task1()
{
    std::cout << "I like singsing" << std::endl;
}

void task2()
{
    std::cout << "I like dancing" << std::endl;
}

void task3()
{
    std::cout << "I like rapping" << std::endl;
}

void task4()
{
    std::cout << "I like playing basketball" << std::endl;
}

void load()
{
    taskList.push_back(task1);
    taskList.push_back(task2);
    taskList.push_back(task3);
    taskList.push_back(task4);
}