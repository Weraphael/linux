// #include <unistd.h>
// #include <iostream>
// #include <vector>
// #include <sys/stat.h>
// #include <sys/wait.h>
// #include <functional>
// #include <ctime>
// #include "tasks.hpp"
// using std::cin;
// using std::cout;
// using std::endl;
// using std::vector;

// extern std::vector<std::function<void()>> taskList;

// const int processNum = 5;

// class Pipe // 描述
// {
// public:
//     Pipe(int cmdfd, pid_t sonid)
//         : _cmdfd(cmdfd), _sonid(sonid)
//     {
//     }

//     int Get_cmdfd() const
//     {
//         return _cmdfd;
//     }

//     int Get_sonid() const
//     {
//         return _sonid;
//     }

// private:
//     int _cmdfd;   // 父进程需要给哪个管道发送任务
//     pid_t _sonid; // 子进程可以通过自己的pid来确定是否是自己需要执行的任务
// };

// void Writer(const vector<Pipe> &pipes)
// {
//     while (true)
//     {
//         // 1. 选择任务
//         int cmdcode = rand() % taskList.size();
//         // 2. 选择进程
//         int processPos = rand() % processNum;
//         // 3. 发布任务
//         write(pipes[processPos].Get_cmdfd(), &cmdcode, sizeof(cmdcode));

//         cout << "我是父进程，我发布的任务码是：" << cmdcode << endl;
//         sleep(1); // 1秒写一次
//     }
// }

// void Reader(int rfd)
// {
//     while (true)
//     {
//         int cmdcode = 0;
//         ssize_t res = read(rfd, &cmdcode, sizeof(cmdcode));
//         if (res == sizeof(int)) // 读取成功
//         {
//             // 根据任务码cmdcode执行任务
//             if (cmdcode >= 0 && cmdcode < taskList.size())
//             {
//                 cout << "我是子进程, 我的id是: " << getpid() << ", 我执行的任务码是：" << cmdcode << ", 我执行的任务是：";
//                 taskList[cmdcode]();
//             }
//         }
//         else if (res == 0) // 写端关闭，读端read返回0，就没必要再进行通信了
//         {
//             break;
//         }
//     }
// }

// void Initprocess(vector<Pipe> *pipes)
// {
//     // 假设进程池里有5个已经创建好的进程
//     for (int i = 0; i < processNum; i++)
//     {
//         // 创建管道
//         int pipefd[2];
//         int n = pipe(pipefd);
//         if (n == -1) // 管道创建失败
//         {
//             perror("pipe");
//         }
//         // 创建子进程
//         pid_t sonpid = fork();
//         if (sonpid == 0) // 子进程
//         {
//             // 子进程要从管道读取任务，那么就需要关闭写端pipefd[1]
//             close(pipefd[1]);
//             Reader(pipefd[0]);
//             exit(0);
//         }
//         else if (sonpid > 0) // 父进程
//         {
//             // 父进程要发送任务，那么就要关不读端pipefd[0]
//             close(pipefd[0]);
//             // 接着再初始化对应的子进程对应的管道字段
//             pipes->push_back(Pipe(pipefd[1], sonpid));
//             // // 发布任务
//             // Writer(pipefd[1]);
//         }
//         else // 子进程创建失败
//         {
//             perror("fork");
//             break;
//         }
//     }
// }

// void Debug(const vector<Pipe> &pipes)
// {
//     for (const auto &e : pipes)
//     {
//         cout << e.Get_cmdfd() << " " << e.Get_sonid() << endl;
//     }
// }

// int main()
// {
//     load();                             // 将所有函数加载到tasksList列表中
//     srand((unsigned int)time(nullptr)); // 生成随机种子
//     vector<Pipe> pipes;                 // 再使用一种数据将管道组织起来
//     // 我们可以封装一个函数创建子进程，并且初始化子进程对应的管道字段
//     Initprocess(&pipes);
//     Writer(pipes);
//     Debug(pipes);

//     return 0;
// }
