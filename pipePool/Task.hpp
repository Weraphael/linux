// .hpp - 将声明和实现放在一起
/*
    ".hpp"文件扩展名通常用于C++中的头文件（header files），这些文件包含了类声明、函数原型以及其他需要在多个源文件中共享的声明和定义。与".h"文件相比，".hpp"文件扩展名反映了它们包含C++代码的特性，因为C++标准允许头文件包含C++代码而不仅仅是C代码。

    使用".hpp"作为头文件的扩展名有助于区分C++代码和C代码，并且也有助于开发者更清晰地理解它们所包含的内容。通常来说，".hpp"文件中会包含类的定义、模板类和函数的实现等等。

    在C++源文件中，可以使用 #include "example.hpp" 来包含这个头文件，并且使用其中定义的类和函数。

    总而言之，".hpp"文件扩展名用于标识包含C++代码的头文件，这有助于组织和管理C++项目中的代码。

*/
#pragma once

#include <iostream>
#include <vector>

typedef void (*task_t)();

void task1()
{
    std::cout << "LOL 刷新日志" << std::endl;
}

void task2()
{
    std::cout << "LOL 刷新野区" << std::endl;
}

void task3()
{
    std::cout << "LOL  检测软件是否更新" << std::endl;
}

void task4()
{
    std::cout << "LOL 用户释放技能" << std::endl;
}

void LoadTasks(std::vector<task_t> *tasks)
{
    tasks->push_back(task1);
    tasks->push_back(task2);
    tasks->push_back(task3);
    tasks->push_back(task4);
}