#include "processBar.h"
#include <unistd.h>
#include <string.h>

const char* flipPat = "|/-\\";
void ProcessBar()
{
    char bar[102];
    memset(bar, '\0', sizeof(bar));
    int i = 0;
    int cnt = 0;
    for (i = 0; i <= 100; i++)
    {
        printf("[%-100s][%d%%][%c]\r", bar, i, flipPat[i % 4]);
        fflush(stdout);
        bar[cnt++] = '=';
        
        if (i + 1 < 100) // i == 100 不存 >
            bar[cnt] = '>';

        usleep(100000);
    }
    printf("\n");
}
